package hello;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
public class SerivcesController {
	

	@Autowired
	private RequestService requestService;
	
	public void setRequestService(RequestService requestService) {
		this.requestService = requestService;
	}
	
    @RequestMapping(value="/test", method = RequestMethod.POST)
    public ReceivedPostRequestData postData(HttpServletRequest request, @RequestBody String body) {
    	Enumeration<String>  headerNames = request.getHeaderNames();
    	Enumeration<String>  paramNames = request.getParameterNames();
    	ArrayList<String> headersListNames = Collections.list(headerNames);
    	ArrayList<String> paramsListNames = Collections.list(paramNames);
    	String headersToSave = getHeaders(headersListNames, request);
    	String paramsToSave = getUrlParams(paramsListNames, request);
    	ReceivedPostRequestData receivedData = new ReceivedPostRequestData();
    	receivedData.setBody(body);
    	receivedData.setParams(paramsToSave);
    	receivedData.setHeaders(headersToSave);
    	receivedData.setPath("/test");
    	receivedData.setMethod("POST");
    	requestService.saveData(receivedData);
    	return receivedData;
    }

    @RequestMapping(value="/test", method = RequestMethod.PUT)
    public ReceivedPostRequestData putData(HttpServletRequest request, @RequestBody String body) {
    	Enumeration<String>  headerNames = request.getHeaderNames();
    	Enumeration<String>  paramNames = request.getParameterNames();
    	ArrayList<String> headersListNames = Collections.list(headerNames);
    	ArrayList<String> paramsListNames = Collections.list(paramNames);
    	String headersToSave = getHeaders(headersListNames, request);
    	String paramsToSave = getUrlParams(paramsListNames, request);
    	ReceivedPostRequestData receivedData = new ReceivedPostRequestData();
    	receivedData.setBody(body);
    	receivedData.setParams(paramsToSave);
    	receivedData.setHeaders(headersToSave);
    	receivedData.setPath("/test");
    	receivedData.setMethod("PUT");
    	requestService.saveData(receivedData);
    	return receivedData;
    }
    
    @RequestMapping(value="/test", method = RequestMethod.DELETE)
    public ReceivedPostRequestData delData(HttpServletRequest request, @RequestBody String body) {
    	Enumeration<String>  headerNames = request.getHeaderNames();
    	Enumeration<String>  paramNames = request.getParameterNames();
    	ArrayList<String> headersListNames = Collections.list(headerNames);
    	ArrayList<String> paramsListNames = Collections.list(paramNames);
    	String headersToSave = getHeaders(headersListNames, request);
    	String paramsToSave = getUrlParams(paramsListNames, request);
    	ReceivedPostRequestData receivedData = new ReceivedPostRequestData();
    	receivedData.setBody(body);
    	receivedData.setParams(paramsToSave);
    	receivedData.setHeaders(headersToSave);
    	receivedData.setPath("/test");
    	receivedData.setMethod("DELETE");
    	requestService.saveData(receivedData);
    	return receivedData;
    }
    
    @RequestMapping(value="/test", method = RequestMethod.PATCH)
    public ReceivedPostRequestData patchData(HttpServletRequest request, @RequestBody String body) {
    	Enumeration<String>  headerNames = request.getHeaderNames();
    	Enumeration<String>  paramNames = request.getParameterNames();
    	ArrayList<String> headersListNames = Collections.list(headerNames);
    	ArrayList<String> paramsListNames = Collections.list(paramNames);
    	String headersToSave = getHeaders(headersListNames, request);
    	String paramsToSave = getUrlParams(paramsListNames, request);
    	ReceivedPostRequestData receivedData = new ReceivedPostRequestData();
    	receivedData.setBody(body);
    	receivedData.setParams(paramsToSave);
    	receivedData.setHeaders(headersToSave);
    	receivedData.setPath("/test");
    	receivedData.setMethod("PATCH");
    	requestService.saveData(receivedData);
    	return receivedData;
    }
    
    @RequestMapping(value="/getlogs", method = RequestMethod.GET)
    public Iterable<ReceivedPostRequestData> getData() {
    	Iterable<ReceivedPostRequestData> responseRaw = requestService.retreiveData(); 
    	return responseRaw;
    }
        
    private String getHeaders (ArrayList<String> headersListNames, HttpServletRequest request){
    	String headers = "";
    	for (String headerKey : headersListNames) {
    		String headerValue = request.getHeader(headerKey);
    		headers = headers + headerKey+":"+ headerValue + System.getProperty("line.separator");
    	}
    	return headers;
    }
    
    private String getUrlParams (ArrayList<String> paramListNames, HttpServletRequest request){
    	String params = "";
    	for (String paramName : paramListNames) {
    		String paramValue = request.getParameter(paramName);
    		params = params +paramName+":"+ paramValue + System.getProperty("line.separator");
    	}
    	return params;
    }
    
    
}