package hello;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ReceivedPostRequestData {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
    private Date date;
    private String body;
    private String headers;
    private String params;
    private String path;
    private String method;

    public ReceivedPostRequestData() {
        this.date = new Date();
     }        
    
    
	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public String getMethod() {
		return method;
	}


	public void setMethod(String method) {
		this.method = method;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getHeaders() {
		return headers;
	}
	public void setHeaders(String headers) {
		this.headers = headers;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	@Override
    public String toString() {
    	return "{\"Date\":\"" + date + "\"BODY\":\"" + body + "\",\"Header\":\""+ headers + params+ "\"}";
    }
    
}