package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequestServiceImpl implements RequestService {
	@Autowired 
	private RequestRepository requestRepository;

	public Iterable<ReceivedPostRequestData> retreiveData() {
		Iterable<ReceivedPostRequestData> dataIterable = requestRepository.findAll();
		return dataIterable;
	}

	public void saveData(ReceivedPostRequestData data) {
		requestRepository.save(data);
	}
	
	

}
