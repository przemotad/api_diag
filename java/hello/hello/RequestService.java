package hello;

import hello.ReceivedPostRequestData;


public interface RequestService {
	public Iterable<ReceivedPostRequestData> retreiveData();
	public void saveData(ReceivedPostRequestData data);
}
