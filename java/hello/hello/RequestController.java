package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;


@Controller
public class RequestController {
	
	@Autowired
	private RequestService requestService;
	
	public void setRequestService(RequestService requestService) {
		this.requestService = requestService;
	}
    
    @RequestMapping(value="/request", method = RequestMethod.GET)
    public String request(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        Iterable<ReceivedPostRequestData> responseRaw = requestService.retreiveData(); 
    	model.addAttribute("list", responseRaw);
        return "request";
    }
    
    
}