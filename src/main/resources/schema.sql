create table received_post_request_data
(
 id integer primary key,
 method varchar(10),
 path varchar(200),
 apartment varchar(100),
 date date not null,
 params varchar (4000),
 headers varchar (4000),
 body varchar (4000)
);
create sequence HIBERNATE_SEQUENCE;